# Name & url #
thinkbedstuy | http://thinkbedstuy.com (domain owned by Miles on Godaddy, can be transferred)

## Description ##
thinkbedstuy is an isomorphic web application built in JavaScript. 
The application's foundation comes from https://github.com/albertogasparin/react-starterkit.

The technology stack includes: ReactJs, Redux, Sass, Koa, RethinkDB & Marko.

## Purpose ##
The intention of the app is to allow users to add, edit and delete "resources"
which are searchable and filterable, along with being mapped to a visual map that a user can navigate.
In this particular case we intended for Bedstuy to be the neighborhood being mapped. A resource can be
any place that serves the community. 

### Important npm packages used ####
* React-Redux (https://github.com/reactjs/react-redux) & React Router (https://www.npmjs.com/package/react-router) are used to handle url mapping and interaction with the API.  
* Koa (https://www.npmjs.com/package/koa) is the HTTP middleware framework used to write the API.  
* React-Redux-Form (https://www.npmjs.com/package/react-redux-form) is used for handling form submission.  
* React-Redux-Session (https://www.npmjs.com/package/redux-react-session) is partially implemented to handle user sessions.  
* Rethinkdbdash (https://github.com/neumino/rethinkdbdash) is used to connect to the database.  
* LeafletJs (https://www.npmjs.com/package/leaflet) was the intended library to be used for the mapping.   

### App Structure ###
* /resources/ (built)  
* /resources/name (built)  
* /resources/name/edit (built)  
* /resources/new (built)  
* /login
* /map

### What's built already ###
* Resources API (GET, DELETE, PUT)
* Resources Routes
* Resource Search
* Resources Front End
* Login Front End

### What needs to be built ###
* Resources Filter
* User Account & Session Implementation
* LeafletJS Implementation

### Additional Docs ###
* Initial Application Spec - https://goo.gl/i925zk
* Community Mapping Form August - https://goo.gl/7iW75Q
* Notes on Work Left to finish - https://goo.gl/pum9jX

### Development ###
To start the app locally:  

1. clone the repo  
2. `npm install`  
3. install rethinkdb (https://www.rethinkdb.com/)  
4. create resources table
5. start rethinkdb  
6. `npm run watch`  
7. App will be running locally at 127.0.0.1:3000  


### Production ###
Reference https://github.com/albertogasparin/react-starterkit for notes on deploying app in production
