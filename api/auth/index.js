import _ from 'lodash';
import authenticate from './authenticate'


const r = require('rethinkdbdash')();
const USERS = r.table('users').run();

function *auth() {
	authenticate(this);
	console.log('auth')
}
function *get() {
	console.log('autheeee')
}

function *getUser() {	
	
	this.body = yield r.table("users").limit(20).run();
	console.log('get users')
}


const LOGIN_API = {
  'POST /login': auth,
  'GET /login': get,
  'GET /users': getUser,
};

export default LOGIN_API;
