// import jwt from 'koa-jwt';

// module.exports = function (ctx) {
//   if (ctx.request.body.password === 'password') {
//     ctx.status = 200;
//     ctx.body = {
//       token: jwt.sign({ role: 'admin' }, process.env.SECRET_AUTH), //Should be the same secret key as the one used is ./jwt.js
//       message: "Successfully logged in!"
//     };
//   } else {
//     ctx.status = ctx.status = 401;
//     ctx.body = {
//       message: "Authentication failed"
//     };
//   }
//   return ctx;
// }


import jwt from 'koa-jwt';
require('dotenv').config({ silent: true });

module.exports = jwt({
  secret: process.env.SECRET_AUTH 
});