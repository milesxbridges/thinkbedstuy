import _ from 'lodash';
import jwt from '../auth/authenticate'
/**
 * This file illustrates how you may map
 * single routes using an exported object
 */

const r = require('rethinkdbdash')();
const RESOURCES = r.table('resources').run();
// const RESOURCES = [
//   { id: 1, text: 'Write tests', completed: false },
//   { id: 2, text: 'Add Redux', completed: true },
// ];

function *all() {
  // let getResourcesAsync = (cb) => {
  //   setTimeout(() => cb(null, RESOURCES), 200);
  // };
  // this.body = yield getResourcesAsync;

  this.body = yield r.table("resources").limit(20).run();
  console.log('get all')
}

function *create() {
  let { resource } = this.request.body;

  console.log('add resource')
  console.log(resource.url)
  //add resource to database   --- need to do 
  r.table('resources').insert(resource).run();

  this.body = yield resource;
}

function *find() {
  //let resource = RESOURCES.find((t) => t.id === this.params.id);
  //console.log(this.params.name)
  this.body = yield r.table("resources").filter({url: this.params.name}).run();
  //this.body = resource;
  console.log('find')
}

//need to create DELETE function
function *del(id) {
  //let { id } = this.request.body;
  console.log(this.params)
  this.body = yield r.table("resources").filter({url: this.params.name}).delete().run();
  console.log('delete')
}

function *put(id) {
  let { resource } = this.request.body;
  console.log(resource)
  //this.body = yield resource;

  this.body = yield r.table("resources").filter({url: resource.url}).update({
    businessName: resource.businessName,
    url: resource.url,
    address: resource.address,
    interviewee: resource.interviewee,
    email: resource.email,
    shortDescription: resource.shortDescription,
    fullDescription: resource.fullDescription,
    website: resource.website,
    keywords: resource.keywords,
    category: resource.category,
    q1: resource.q1,
    q2: resource.q2,
    q3: resource.q3,
    q4: resource.q4,
    q5: resource.q5,
    q6: resource.q6,
    q7: resource.q7,
    q8: resource.q8,
    q9: resource.q9,
    q10: resource.q10,
    q11: resource.q11,
    q12: resource.q12,
    q13: resource.q13,
    q14: resource.q14,
    q15: resource.q15,
    q16: resource.q16,
    q17: resource.q17,
    q18: resource.q18,
    q19: resource.q19,
    q20: resource.q20,
    q21: resource.q21,
  }).run();
}

function *pub(id) {
  // let { resource } = this.request.body;
  console.log('publish')
  //this.body = yield resource;

  // this.body = yield r.table("resources").filter({url: resource.url}).update({
  //   publish: resource.publish,

  // }).run();
}


function *search(query) {
  let  q = (this.params.name=="all") ? '.' : `(?i)`+this.params.name

  
  this.body = yield r.table("resources").filter(function(resource) {
    let sponsored = resource("businessName").match(q);
    let genre = resource("category").match(q);
    console.log(q)
    return sponsored;

    
  }).run();
}


const API = {
  'GET /resources': all,
  'POST /resources': create,
  'GET /resources/:name': find,
  'DELETE /resources/:name': del,
  'PUT /resources/edit': put,
  'PUT /resources/publish': pub,
  'GET /resources/search/:name': search,
};

export default API;
export { RESOURCES };
