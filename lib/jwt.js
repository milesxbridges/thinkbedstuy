import jwt from 'koa-jwt';
require('dotenv').config({ silent: true });

module.exports = jwt({
  secret: process.env.SECRET_AUTH 
});