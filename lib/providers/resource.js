/**
 * Server side provider
 */

import co from 'co';
import { types, reducer, actions as _actions } from '../../app/providers/resource';

// method that returns resources from DB
import { RESOURCES } from '../../api/resources';
const getResourcesAsync = (cb) => {
  setTimeout(() => cb(null, RESOURCES), 200);
};

/**
 * Actions overrides
 */

const actions = {
  // Preserve all actions
  ..._actions,

  // Override async/fetch actions
  loadAsync() {
    return (dispatch, getState) => {
      // Called on server side rendering,
      // using co we return a promise and we can yield
      return co.wrap(function *() {
        let resources = yield getResourcesAsync;
        dispatch(actions.load(resources));
      })();
    };
  },

};


// Export same objects as app/providers
export { types, reducer, actions };
