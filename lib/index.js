/**
 * Dependencies
 */
import koa from 'koa';
import rethinkdbdash from 'rethinkdbdash';

import config from './config';
import koaSetup from './koa';
import koaRouterSetup from './router';

/**
 * Server
 */
const app = new koa();

// setup webpack middlewares
// enables webpack serving + React hot reload
if (config.env === 'development') {
  let webpackSetup = require('./webpack').default;
  webpackSetup(app);
}

// setup app middlewares
// like static serving, error handling, templates engine
koaSetup(app);

// enhance require
// to be better compatible w/ React - Webpack env
if (config.isomorphic) {
  require('./react-compat');
}

// setup router
// with routes matching for /api and react router
koaRouterSetup(app);

export default app;
