import React from 'react';
import { Route, IndexRoute } from 'react-router';
import { sessionService } from 'redux-react-session';

import App from './components/App';
import Resources from './components/Resources';
import Resource from './components/Resource';
import ResourceForm from './components/ResourceForm';
import ResourceEdit from './components/ResourceEdit';
import Login from './components/Login';
import NotFound from './components/NotFound';


/**
 * Routes for both server and client
 */

export default (
  <Route path="/" component={App}>
  	<IndexRoute component={Login} />
  	<Route path="login" component={Login} />
    <Route path="resources" component={Resources} />
    <Route path="resources/new" component={ResourceForm} />
    <Route path="resources/:name" component={Resource} />
    <Route path="resources/:name/edit" component={ResourceEdit} />
    <Route path="*" component={NotFound} />
  </Route>
);

// function requireAuth(nextState, replace) {  
//   if (!sessionStorage.jwt) {
//     replace({
//       pathname: '/login',
//       state: { nextPathname: nextState.location.pathname } onEnter={requireAuth} 
//     })
//   }
// }


function authenticate() {  
  console.log(sessionService)
}

// onEnter={sessionService.checkAuth}
// onEnter={sessionService.checkAuth}
// onEnter={sessionService.checkAuth}
