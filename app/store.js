import { createStore, applyMiddleware, compose } from 'redux';
import { sessionService } from 'redux-react-session';
import thunk from 'redux-thunk';
import reducers from 'providers';

export let store;


//default form state
const initialState = {
  resources: []
};

export default function (initialState, thunkMiddleware = thunk) {
  // Enhance redux with middlewares and other enhancers
  const enhancer = compose(
    applyMiddleware(
      // async mw
      thunkMiddleware,
      // redux-immutable-state-invariant mw (DEV only)
      __CLIENT__ && window.reduxImmutable ? window.reduxImmutable() : ((s) => (n) => (a) => n(a))
    ),
    // support Chrome redux-devtools-extension
    __CLIENT__ && window.devToolsExtension ? window.devToolsExtension() : (f) => f
  );

  store = createStore(reducers, initialState, enhancer);

  store.subscribe(() => {
    console.log(store.getState());
  }); 

  const options = { refreshOnCheckAuth: true, redirectPath: '/', driver: 'COOKIES' };
  sessionService.initSessionService(store, options)
    .then(() => console.log('Redux React Session is ready and a session was refreshed from your storage'))
    .catch(() => console.log('Redux React Session is ready and there is no session in your storage'))

  return store;
}
