import React, { Component } from 'react';
import { Router, Route, Link, browserHistory } from 'react-router';
import './style.scss';

class Footer extends Component {

  render() {
    return (
       	<footer>
       		<nav>
  	    		<h6><Link to="/">developed</Link></h6>
  	    	</nav>
    	</footer>
    );
  }
}

export default Footer;
