import React, { Component } from 'react';
import { Router, Route, Link, browserHistory } from 'react-router';
import './style.scss';

class Header extends Component {

  render() {
    return (
       	<header>
       		<nav>
	    		<h1><Link to="/resources">thinkbedstuy</Link></h1>
	    	</nav>
        <div className="control-bar">
          <Link to="/login" className="lilo">Login</Link>
        </div>
    	</header>
    );
  }
}

export default Header;
