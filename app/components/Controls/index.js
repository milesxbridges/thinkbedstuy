import React, { Component } from 'react';
import { Router, Route, Link, browserHistory } from 'react-router';
import './style.scss';

class Controls extends Component {

  render() {
    return (
       	<div className="control-bar">
          <Link to="/resources/new" className="lilo">Add Resource</Link>
        </div>
    );
  }
}

export default Controls;
