import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Control, Errors, Form, actions } from 'react-redux-form';
import * as providers from 'providers';
import $ from 'jquery';



import './style.scss';



var test = {

  paddingTop: "100px",
};

class Login extends Component {
  constructor(props) {
    super(props);

  }


  componentDidMount() { 
    let { actions } = this.props;
    //actions.authenticate.check()
   
   console.log('did mount')
  //  console.log(this.props)

  }
  componentWillMount() {
    // let { resources, actions } = this.props;
   // actions.resource.resetResource();
   
  }
  componentDidUpdate() {
    // let { resources, actions } = this.props;
    // // console.log('waaa')
    // // console.log(resources)
    // console.log(this.props)
    
  }
  handleSubmit(val) {
    // Do anything you want with the form value
    let { actions } = this.props;
    
    actions.authenticate.login(val);
    
   // console.log(val)

  }

  handleChange(val) {
   //  let { actions } = this.props;
    
   // // actions.resource.addAsync(val);
   //  // console.log('change')
   //  console.log(val)
  }
  handleUpdate() {
    // console.log('updating')


  }


  render() {
    // let { resource } = this.props; 


    return(
      <div className="forms">
        <div className="resource-form"> 
          <Form
            model="deep.login" 
            onSubmit={(val) => this.handleSubmit(val)}
            onChange={(val) => this.handleChange(val)}
            onUpdate={(val) => this.handleUpdate(val)}
            validateOn="change"
          >
            

            <div
              className=''
              >
         
              <div className="field">
                <label>username:</label>
                <Control.text model=".username" 
                />
                <Errors
                  model=".username"
                  messages = {{
                    required: 'required'
                  }}
                />
              </div>
              <div className="field">
                <label>password</label>
                <Control type="password" model=".password" />
                <Errors
                  model=".password"
                  messages = {{
                    required: 'required'
                  }}
                />
              </div>

              
            </div>      
            
          
            <button>Log in</button>
          </Form>       
          
          
        </div>
        
      </div>
    );
  }
}

// export default Login;
export default connect(
  (state) => ({ auth: state.auth }),
  (dispatch) => ({
    actions: {
     authenticate: bindActionCreators(providers.authenticate.actions, dispatch),
    },
  })
)(Login);



