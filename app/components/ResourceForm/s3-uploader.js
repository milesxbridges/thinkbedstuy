import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import DropzoneS3Uploader from 'react-dropzone-s3-uploader';
 

class s3Uploader extends Component {
 
 
  render() {
    const uploadOptions = {
      server: 'http://127.0.0.1:3000',
      s3Url: 'https://thinkbedstuy.s3.amazonaws.com',
      signingUrlQueryParams: {uploadType: 'avatar'},
    }
 
    return (
      <DropzoneS3Uploader 
        maxSize={1024 * 1024 * 5}
        upload={uploadOptions}
      />
    )
  }
}

export default connect(
  (state) => ({ resources: state.resources }),
  (dispatch) => ({
    actions: {
     resource: bindActionCreators(providers.resource.actions, dispatch),
    },
  })
)(s3Uploader);
