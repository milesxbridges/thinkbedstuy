import PropTypes from 'prop-types';
import React from 'react';
import { Router, Route, Link, browserHistory } from 'react-router';
/**
 * ResourceItem component
 */

const propTypes = {
	id: PropTypes.string,
	businessName: PropTypes.string,
	url: PropTypes.string,
	address: PropTypes.string,
	interviewee: PropTypes.string,
	shortDescription: PropTypes.string,
	fullDescription: PropTypes.string,
	website: PropTypes.string,
	keywords: PropTypes.string,
	category: PropTypes.string,
};

const ResourceItem = ({ id, businessName, url, address, shortDescription, fullDescription, website, keywords, category }) => (
	<div className="ResourceItem col-large-4 col-medium-6">
		<div className="wrap">
			<Link to={"/resources/" + url} className="img">
				<img src="http://lorempixel.com/600/350" />
			</Link>
			<div className="text">
				<h6>{category}</h6>
				<h2><Link to={"/resources/" + url}>{businessName}</Link></h2>
				<h3>{address} </h3>
				<p>{shortDescription}</p>
				<h4><a href="">{website}</a></h4>
				
			</div>
		</div>
	</div>
);

ResourceItem.propTypes = propTypes;

export default ResourceItem;