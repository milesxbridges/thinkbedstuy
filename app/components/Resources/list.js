import PropTypes from 'prop-types';
import React from 'react';
import ReduxInfiniteScroll from 'redux-infinite-scroll';
import _ from 'lodash';

import ResourceItem from './item';

/**
 * ResourceList component
 */

const propTypes = {
  resources: PropTypes.array,
};

const ResourceList = ({ resources = [] }) => (
	
	<div className="row resources-list">
		{_.map(resources, (resource, i) => (
			<ResourceItem key={i} {...resource} />
		))}

	</div>
);

ResourceList.propTypes = propTypes;

export default ResourceList;