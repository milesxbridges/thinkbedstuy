import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Control, Errors, Form, actions } from 'react-redux-form';
import * as providers from 'providers';
/**
 * Search component
 */



class Search extends Component {
  constructor(props) {
    super(props);
    
  }
  componentDidMount() {
    let { actions } = this.props;
    //actions.resource.loadAsync();
   //console.log(resources)
  // console.log('hey')
  }

  componentWillMount() {
    let { resources, actions } = this.props;
   

  }
  handleSubmit(val) {
    // Do anything you want with the form value
    // let { actions } = this.props;
    
    // actions.resource.addAsync(val);
    // console.log('add new resource')

  }

  handleChange(val) {
  	let { resources, actions } = this.props;
    actions.resource.filterResource(val)


  }
  handleUpdate(val) {
  	let { resources, actions } = this.props;
    // console.log('updating')
  	


  }

  render () {
    return (
    	<Form
            model="deep.search" 
            onSubmit={(val) => this.handleSubmit(val)}
            onChange={(val) => this.handleChange(val)}
            onUpdate={(val) => this.handleUpdate(val)}
        >
        	<Control.text 
        		model=".query" 
        		className="search"
        		placeholder="search for a resource in the bedstuy community"
        	/>
        </Form>
      
    );
  }
}

export default connect(
  (state) => ({ resources: state.resources }),
  (dispatch) => ({
    actions: {
      resource: bindActionCreators(providers.resource.actions, dispatch),
    },
  })
)(Search);