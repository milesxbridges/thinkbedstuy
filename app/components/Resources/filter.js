import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Control, Errors, Form, actions } from 'react-redux-form';
import * as providers from 'providers';
/**
 * Filter component
 */



class Filter extends Component {
  constructor(props) {
    super(props);
    
  }
  componentDidMount() {
    let { actions } = this.props;
    //actions.resource.loadAsync();
   //console.log(resources)
  // console.log('hey')
  }

  componentWillMount() {
    let { resources, actions } = this.props;
   

  }
  handleSubmit(val) {
    // Do anything you want with the form value
    // let { actions } = this.props;
    
    // actions.resource.addAsync(val);
    // console.log('add new resource')

  }

  handleChange(val) {
  	let { resources, actions } = this.props;
    actions.resource.filterResource(val)


  }
  handleUpdate(val) {
  	let { resources, actions } = this.props;
    // console.log('updating')
  	


  }

  render () {
    return (
    	<Form
        model="deep.filter" 
        onSubmit={(val) => this.handleSubmit(val)}
        onChange={(val) => this.handleChange(val)}
        onUpdate={(val) => this.handleUpdate(val)}
        >
        	<Control.select model=".category">
            <option value=""></option>
            <option value="Arts, crafts, and collectibles">Arts, crafts, and collectibles</option>
                <option value="Baby">Baby</option>
                <option value="Beauty and fragrances">Beauty and fragrances</option>
                <option value="Books and magazines">Books and magazines</option>
                <option value="Business to business">Business to business</option>
                <option value="Clothing, accessories, and shoes">Clothing, accessories, and shoes</option>
                <option value="Computers, accessories, and services">Computers, accessories, and services</option>
                <option value="Education">Education</option>
                <option value="Electronics and telecom">Electronics and telecom</option>
                <option value="Entertainment and media">Entertainment and media</option>
                <option value="Financial services and products">Financial services and products</option>
                <option value="Food retail and service">Food retail and service</option>
                <option value="Gifts and flowers">Gifts and flowers</option>
                <option value="Government">Government</option>
                <option value="Health and personal care">Health and personal care</option>
                <option value="Home and garden<">Home and garden</option>
                <option value="Nonprofit">Nonprofit</option>
                <option value="Pets and animals">Pets and animals</option>
                <option value="Religion and spirituality (for profit)">Religion and spirituality (for profit)</option>
                <option value="Retail (not elsewhere classified)">Retail (not elsewhere classified)</option>
                <option value="Services - other">Services - other</option>
                <option value="Sports and outdoors">Sports and outdoors</option>
                <option value="Toys and hobbies">Toys and hobbies</option>
                <option value="Travel">Travel</option>
                <option value="Vehicle sales">Vehicle sales</option>
                <option value="Vehicle service and accessories">Vehicle service and accessories</option>
        	
        	</Control.select>
      </Form>
      
    );
  }
}

export default connect(
  (state) => ({ resources: state.resources }),
  (dispatch) => ({
    actions: {
      resource: bindActionCreators(providers.resource.actions, dispatch),
    },
  })
)(Filter);