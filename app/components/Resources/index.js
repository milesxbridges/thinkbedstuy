import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as providers from 'providers';
import ResourceList from './list';
import Search from './search';
import './style.scss';

import 'assets/icons/add.svg';

/**
 * Resources wrapper component
 */

class Resources extends Component {

  constructor(props) {
    super(props);
    
  }
  componentDidMount() {
    let { actions } = this.props;
    actions.resource.loadAsync();
   //console.log(resources)
  // console.log('hey')
  }

  componentWillMount() {
    let { resources, actions } = this.props;
    if (!resources.length) {
      actions.resource.loadAsync();
    }
  }

  render () {
    return (
      <div className="Resources main-wrap">
        <Search />
        <ResourceList {...this.props} />
       
      </div>
    );
  }
}

export default connect(
  (state) => ({ resources: state.resources }),
  (dispatch) => ({
    actions: {
      resource: bindActionCreators(providers.resource.actions, dispatch),
    },
  })
)(Resources);
