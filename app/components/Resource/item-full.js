import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';
/**
 * ResourceItemFull component
 */
import ResourceItem from './item';



const propTypes = {
  resources: PropTypes.array,
};



const ResourceItemFull = ({ resources = [] }) => (
	
	<div className="row resource">
		{_.map(resources, (resource, i) => (
			<ResourceItem key={i} {...resource} />
		))}

	</div>

);

ResourceItemFull.propTypes = propTypes;

export default ResourceItemFull;