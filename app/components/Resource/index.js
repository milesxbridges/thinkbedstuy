import React, { Component } from 'react';
import { Link } from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import $ from 'jquery'

import * as providers from 'providers';
import ResourceItemFull from './item-full';

import './style.scss';


var test = {

  paddingTop: "100px",
};

class Resource extends Component {

	constructor(props) {
		super(props);

		this.state = {
			confirm: false
		}
		this.delR = this.delR.bind(this)
		//this.toggleMi = this.toggleMi.bind(this)


		//console.log()

	}
	componentDidMount() {
		let { actions } = this.props;
		let p = this.props.location.pathname;
		actions.resource.loadResourceAsync(p);

	}
	componentWillMount() {

		
	}
	delR() {

		let { actions } = this.props;
		let p = this.props.location.pathname;
     	actions.resource.deleteResource(p);
	}



	render() {
			//console.log(state.resources)
		return(
			<div className="main-wrap col-12">
				<ResourceItemFull {...this.props} />
				<div className="delete" onClick={this.delR}>delete</div>	
			</div>
		);
	}
}


export default connect(
  (state) => ({ resources: state.resources }),
  (dispatch) => ({
    actions: {
      resource: bindActionCreators(providers.resource.actions, dispatch),
    },
  })
)(Resource);

