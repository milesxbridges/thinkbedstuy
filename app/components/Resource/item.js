import PropTypes from 'prop-types';
import React from 'react';
import { Router, Route, Link, browserHistory } from 'react-router';
/**
 * ResourceItem component
 */



const propTypes = {
	id: PropTypes.string,
	businessName: PropTypes.string,
	url: PropTypes.string,
	address: PropTypes.string,
	interviewee: PropTypes.string,
	shortDescription: PropTypes.string,
	fullDescription: PropTypes.string,
	website: PropTypes.string,
	keywords: PropTypes.string,
	category: PropTypes.string,
	q1: PropTypes.string,
	q2: PropTypes.string,
	q3: PropTypes.string,
	q4: PropTypes.string,
	q5: PropTypes.string,
	q6: PropTypes.string,
	q7: PropTypes.string,
	q8: PropTypes.string,
	q9: PropTypes.string,
	q10: PropTypes.string,
	q11: PropTypes.string,
	q12: PropTypes.string,
	q13: PropTypes.string,
	q14: PropTypes.string,
	q15: PropTypes.string,
	q16: PropTypes.string,
	q17: PropTypes.string,
	q18: PropTypes.string,
	q19: PropTypes.string,
	q20: PropTypes.string,
	q21: PropTypes.string,
};
const ResourceItem = ({ id, businessName, url, address, shortDescription, fullDescription, website, keywords, category, 
	q1,
	q2,
	q3,
	q4,
	q5,
	q6,
	q7,
	q8,
	q9,
	q10,
	q11,
	q12,
	q13,
	q14,
	q15,
	q16,
	q17,
	q18,
	q19,
	q20,
	q21,
 }) => (
	<div className="ResourceItem col-12">
		<div className="controls">
			<Link to={"/resources/" + url + "/edit"} >
				edit
			</Link>
		</div>
		<div className="bi">
			<div className="img">
				<img src="http://lorempixel.com/900/450" />
			</div>
			<div className="text">
				<h6>{category} </h6>
				<h2>{businessName} </h2>
				<h3>{address} </h3>
				<p>{fullDescription} </p>
				<h4><a href="">{website}</a></h4>
				<h5>{keywords} </h5>
			</div>
			
		</div>
		<div className="qu">
			<h3>More Information</h3>
			<div className="text">
			
				{ q1 ? 
					<ul>
						<li>How long have you been open?</li>
						<li>{q1}</li>
					</ul>
					: 
					''
				}
				{ q2 ? 
					<ul>
						<li>What do you offer, or sell, or make here?</li>
						<li>{q2}</li>
					</ul>
					: ''
				}
				{q3 ? 
					<ul>
						<li>How many people work here?</li>
						<li>{q3}</li>
					</ul>
					: ''
				}
				{q4 ? 
					<ul>
						<li>How do you find and hire employees?</li>
						<li>{q4}</li>
					</ul>
					: ''
				}
				{q5 ? 
					<ul>
						<li>How do you attract your clientle?</li>
						<li>{q5}</li>
					</ul>
					: ''
				}
				{q6 ? 
					<ul>
						<li>Who do you reach at your place of business?</li>
						<li>{q6}</li>
					</ul>
					: ''
				}
				{q7 ? 
					<ul>
						<li>How would you characterize the people you reach? What adjectives or words come to mind? </li>
						<li>{q7}</li>
					</ul>
					: ''
				}
				{q8 ? 
					<ul>
						<li>What percentage of the Bed-Stuy community does your place serve/reach?</li>
						<li>{q8}</li>
					</ul>
					: ''
				}
				{q9 ? 
					<ul>
						<li>How do you advertise?</li>
						<li>{q9}</li>
					</ul>
					: ''
				}
				{q10 ? 
					<ul>
						<li>Can individuals walk in or do they need to make an appointment?</li>
						<li>{q10}</li>
					</ul>
					: ''
				}
				{q11 ? 
					<ul>
						<li>Are your staff members/employees from Bed-Stuy?</li>
						<li>{q11}</li>
					</ul>
					: ''
				}
				{q12 ? 
					<ul>
						<li>How do you give back to the Bed Stuy Community?</li>
						<li>{q12}</li>
					</ul>
					: ''
				}
				{q13 ? 
					<ul>
						<li>What would it take for you to hire a young person from Bed-Stuy who may have had run-in’s with the law?</li>
						<li>{q13}</li>
					</ul>
					: ''
				}
				{q14 ? 
					<ul>
						<li>Would you be willing to provide on the job training for Bed-Stuy youth/young adults if it is paid for by someone else?</li>
						<li>{q14}</li>
					</ul>
					: ''
				}
				{q15 ? 
					<ul>
						<li>And would you be willing to hire them once they complete the training satisfactorily?</li>
						<li>{q15}</li>
					</ul>
					: ''
				}
				{q16 ? 
					<ul>
						<li>Would you be interested in learning more about the Bed-Stuy Human Justice Community Council that works to develop skills and resources for the youth and residents of Bedford Stuyvesant?</li>
						<li>{q16}</li>
					</ul>
					: ''
				}
				{q17 ? 
					<ul>
						<li>Would you be willing to host and/or co-host events with the Bed-Stuy Human Justice Community Council to support youth development in Bedford Stuyvesant?</li>
						<li>{q17}</li>
					</ul>
					: ''
				}
				{q18 ? 
					<ul>
						<li>Would you be willing to attend meetings, community events, and art shows hosted by the Bed-Stuy Human Justice Community Council to support youth development in Bedford Stuyvesant?</li>
						<li>{q18}</li>
					</ul>
					: ''
				}
				{q19 ? 
					<ul>
						<li>What are gifts, talents and resources you can offer to the Bed-Stuy Human Justice Community Council to support youth development in Bedford Stuyvesant?</li>
						<li>{q19}</li>
					</ul>
					: ''
				}
				{q20 ? 
					<ul>
						<li>What is a memorable place in Bedford Stuyvesant for you or a memorable story about time in Bedford Stuyvesant?</li>
						<li>{q20}</li>
					</ul>
					: ''
				}
				{q21 ? 
					<ul>
						<li>Do you see yourself and your business/organization/institutions still to be located here in Bedford Stuyvesant 10 years from now?</li>
						<li>{q21}</li>
					</ul>
					: ''
				}

			</div>
		</div>
		
	</div>

);

ResourceItem.propTypes = propTypes;

export default ResourceItem;