import React, { Component } from 'react';


import './style.scss';
import './the-flex-grid.scss';

import Header from '../Header';
import Footer from '../Footer';

/**
 * App component
 */

const App = ({ children }) => (
  <div className="App">
    <Header />
    <section>
      {children}
    </section>
    <Footer />
  </div>
);

export default App;
