import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Control, Form, actions } from 'react-redux-form';
import { Editor } from 'react-draft-wysiwyg';
import * as providers from 'providers';
import $ from 'jquery';


import PublishToggle from './publish-toggle';
import './style.scss';



var test = {

  paddingTop: "100px",
};

class ResourceEdit extends Component {
  constructor(props) {
    let change = actions;
    super(props);

   this.state = {
      activeBi: true,
      activeQu: false,
      activeIm: false
    }
    this.handleUpdate = this.handleUpdate.bind(this)
    this.toggleQu = this.toggleQu.bind(this)
    this.toggleBi = this.toggleBi.bind(this)
    this.toggleIm = this.toggleIm.bind(this)
    this.publishToggle = this.publishToggle.bind(this)
    //let { change } = this.props;
    
  
  }


  componentDidMount() { 
    let { resources, actions, change } = this.props;
    let p = `/` + this.props.location.pathname.split('/')[2]; 

    actions.resource.loadResourceAsync(`/resources`+ p);
  
   
 

  }
  componentDidUpdate() {
    let { resources, actions } = this.props;
    actions.resource.loadAsyncEdit(resources[0])
    console.log('000')
    console.log(resources[0])
    
  }
  componentWillMount() {


   
  }
  componentWillReceiveProps()  {


  }
  handleSubmit(val) {
    // Do anything you want with the form value
    

  }

  handleChange(val) {
    let { actions } = this.props;
    actions.resource.editAsync(val);
    console.log(val)
    

  }
  handleUpdate() {
    


  }
  publishToggle(val) {
    // let { actions } = this.props;
      
    // actions.resource.publishToggle('val');
    console.log('publish')
    
  }

  //ui interaction
  toggleBi() {
    this.setState({
      activeBi: true,
      activeQu: false, 
      activeIm: false
    })
  }
  toggleQu() {
    this.setState({
      activeQu: true,
      activeBi: false,
      activeIm: false
    })
  }
  toggleIm() {
    this.setState({
      activeIm: true,
      activeQu: false,
      activeBi: false
    })
  }

  render() {
    let { resource } = this.props; 

    const required = (val) => val && val.length;

    return(
      <div className="forms">
        <div className="resource-edit">
          <nav>
            <ul>
              <li 
                onClick={this.toggleBi}
                className={this.state.activeBi ? 'a' : ''}>Basic Information</li>
              <li
                onClick={this.toggleQu}
                className={this.state.activeQu ? 'a' : ''}>Questionnaire</li>
              <li
                onClick={this.toggleIm}
                className={this.state.activeIm ? 'a' : ''}>Images</li>
            </ul>
          </nav>
          <Form
            model="deep.resourceEdit" 
            onSubmit={(val) => this.handleSubmit(val)}
            onChange={(val) => this.handleChange(val)}
            onUpdate={(val) => this.handleUpdate(val)}

          >
            <div
              className={this.state.activeBi ? 'basic-info a' : 'basic-info'}
              >
              <label>Business Name</label>
              <Control.text 
                validators={{ required }}
                model=".businessName"  
           
              />
              <label>URL</label>
              <Control.text 
                validators={{ required }}
                model=".url" 
                className="url" 
              />
             
              <label>Address</label>
              <Control.text model=".address" />
              <label>Full Name of person you are talking to </label>
              <Control.text model=".interviewee" />
              <label>Email</label>
              <Control.text type="email" model=".email" />
              <label>Short Description</label>

              <Control.textarea model=".shortDescription" />
              <label>Full Description</label>
              <Control.textarea model=".fullDescription" />
              <label>Website</label>
              <Control.text type="email" model=".website" />
              <label>Keywords</label>
              <Control.text model=".keywords" />
              <label>Category</label>
              <Control.select model=".category">
                <option value=""></option>
                <option value="Arts, crafts, and collectibles">Arts, crafts, and collectibles</option>
                <option value="Baby">Baby</option>
                <option value="Beauty and fragrances">Beauty and fragrances</option>
                <option value="Books and magazines">Books and magazines</option>
                <option value="Business to business">Business to business</option>
                <option value="Clothing, accessories, and shoes">Clothing, accessories, and shoes</option>
                <option value="Computers, accessories, and services">Computers, accessories, and services</option>
                <option value="Education">Education</option>
                <option value="Electronics and telecom">Electronics and telecom</option>
                <option value="Entertainment and media">Entertainment and media</option>
                <option value="Financial services and products">Financial services and products</option>
                <option value="Food retail and service">Food retail and service</option>
                <option value="Gifts and flowers">Gifts and flowers</option>
                <option value="Government">Government</option>
                <option value="Health and personal care">Health and personal care</option>
                <option value="Home and garden<">Home and garden</option>
                <option value="Nonprofit">Nonprofit</option>
                <option value="Pets and animals">Pets and animals</option>
                <option value="Religion and spirituality (for profit)">Religion and spirituality (for profit)</option>
                <option value="Retail (not elsewhere classified)">Retail (not elsewhere classified)</option>
                <option value="Services - other">Services - other</option>
                <option value="Sports and outdoors">Sports and outdoors</option>
                <option value="Toys and hobbies">Toys and hobbies</option>
                <option value="Travel">Travel</option>
                <option value="Vehicle sales">Vehicle sales</option>
                <option value="Vehicle service and accessories">Vehicle service and accessories</option>
              </Control.select>
            </div>      
            <div 
              className={this.state.activeQu ? 'questionnaire a' : 'questionnaire'}>
              <label>How long have you been open?</label>
              <Control.text model=".q1" />
              <label>What do you offer, or sell, or make here?</label>
              <Control.text model=".q2" className="url" />
              <label>How many people work here?</label>
              <Control.text model=".q3" />
              <label>How do you find and hire employees?</label>
              <Control.text model=".q4" />
              <label>How do you attract your clientle?</label>
              <Control.textarea model=".q5" />
              <label>Who do you reach at your place of business?</label>
              <Control.textarea model=".q6" />
              <label>
                How would you characterize the people you reach?
                What adjectives or words come to mind? 
              </label>
              <Control.text type="email" model=".q7" />
              <label>What percentage of the Bed-Stuy community does your place serve/reach?</label>
              <Control.text model=".q8" />

              <label>How do you advertise?</label>
              <Control.text model=".q9" />
              <label>Can individuals walk in or do they need to make an appointment?</label>
              <Control.text model=".q10" />
              <label>Are your staff members/employees from Bed-Stuy?</label>
              <Control.text model=".q11" />
              <label>How do you give back to the Bed Stuy Community?</label>
              <Control.text model=".q12" />
              <label>What would it take for you to hire a young person from Bed-Stuy who may have had run-in’s with the law?</label>
              <Control.text model=".q13" />
              <label>Would you be willing to provide on the job training for Bed-Stuy youth/young adults if it is paid for by someone else?</label>
              <Control.text model=".q14" />
              <label>And would you be willing to hire them once they complete the training satisfactorily? </label>
              <Control.text model=".q15" />
              <label>Would you be interested in learning more about the Bed-Stuy Human Justice Community Council that works to develop skills and resources for the youth and residents of Bedford Stuyvesant?</label>
              <Control.text model=".q16" />
              <label>Would you be willing to host and/or co-host events with the Bed-Stuy Human Justice Community Council to support youth development in Bedford Stuyvesant?</label>
              <Control.text model=".q17" />
              <label>Would you be willing to attend meetings, community events, and art shows hosted by the Bed-Stuy Human Justice Community Council to support youth development in Bedford Stuyvesant?</label>
              <Control.text model=".q18" />
              <label>What are gifts, talents and resources you can offer to the Bed-Stuy Human Justice Community Council to support youth development in Bedford Stuyvesant?</label>
              <Control.text model=".q19" />
              <label>What is a memorable place in Bedford Stuyvesant for you or a memorable story about time in Bedford Stuyvesant?</label>
              <Control.text model=".q20" />
              <label>Do you see yourself and your business/organization/institutions still to be located here in Bedford Stuyvesant 10 years from now?</label>
              <Control.text model=".q21" />
                
            </div>
            <div
              className={this.state.activeIm ? 'images a' : 'images'}>
              Images

            </div>
            <Control
              model=".publish"
              component={PublishToggle}
              onClick={this.publishToggle}
            />
          </Form>
          
        </div>
        
      </div>
    );
  }
}

export default connect(
  (state) => ({ resources: state.resources }),
  (dispatch) => ({
    actions: {
     resource: bindActionCreators(providers.resource.actions, dispatch),
    },
  })
)(ResourceEdit);



