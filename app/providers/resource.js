/* eslint-disable complexity */
import _ from 'lodash';
import { createAction, handleActions } from 'redux-actions';
import { track, actions as act } from 'react-redux-form';
import { browserHistory } from 'react-router'


/**
 * Action types
 */

const types = {
  LOAD: 'RESOURCE_LOAD',
  ADD: 'RESOURCE_ADD',
  REMOVE: 'RESOURCE_REMOVE',
  EDIT: 'RESOURCE_EDIT',
  RESET: 'RESOURCE_RESET'
};

/**
 * Reducer
 */

const defaultState = {};

const reducer = handleActions({

  [types.LOAD](state, { payload }) {
    return payload;
  },

  [types.ADD](state, { payload }) {
    //return state.concat(payload);
    return state
    console.log(state)
  },

  [types.REMOVE](state, { payload }) {
    return _.reject(state, { id: payload });
  },
  [types.EDIT](state, {payload}) {
    return state.resources[0].concat(payload);
  },
  [types.RESET](state, { payload }) {
    return payload;
  }

}, defaultState);


/**
 * Actions
 */

const actions = {

  load: createAction(types.LOAD),

  add: createAction(types.ADD),

  remove: createAction(types.REMOVE),

  edit: createAction(types.EDIT),

  reset: createAction(types.RESET),

  resetResource() {
    return (dispatch) => {
      dispatch(act.reset('deep.resourceEdit'));
    }  
  },
  loadAsync() {
    // redux-thunk magic: it allow actions to return functions
    // w/ dispatch as argument, so we can call it as many times as needed
    return (dispatch) => {
      window.fetch(CONFIG_CLIENT.publicPath + 'api/resources', CONFIG_CLIENT.fetch)
      .then((response) => response.json())
      .then((json) => {
        // add resources fetched from the server
        dispatch(actions.load(json));
      });
    };
  },
  loadAsyncEdit(re) {
    return (dispatch) => {
      dispatch(act.change('deep.resourceEdit',re));

    };
  },
  loadResourceAsync(p) {
    // redux-thunk magic: it allow actions to return functions
    // w/ dispatch as argument, so we can call it as many times as needed
    return (dispatch) => {
      window.fetch(CONFIG_CLIENT.publicPath + 'api' +  p, CONFIG_CLIENT.fetch)
      .then((response) => response.json())
      .then((json) => {
        // add resources fetched from the server
        dispatch(actions.load(json));
        //return json
      });
    };
  },
  deleteResource(p) {

    return (dispatch) => {
     // 

      window.fetch(CONFIG_CLIENT.publicPath + 'api' +  p, {
        ...CONFIG_CLIENT.delete,
        method: 'delete',
      })
      .then((response) => response.json())
      .then((json) => {
        //console.log(json)
        dispatch(actions.remove(id));
        browserHistory.push('/resources/')
        
      })
      .catch((err)=> {
     //   console.log(err.message)
      })
    };
  },
  addAsync(resource) {

    return (dispatch) => {
      // optimisticly add resource
      let tmpResource = { id: 0, name: `${name}` };
      dispatch(actions.add(tmpResource));

      window.fetch('/api/resources', {
        ...CONFIG_CLIENT.fetch,
        method: 'post',
        body: JSON.stringify({ resource }),
      })
      .then((response) => response.json())
      .then((json) => {
        // remove optimistic resource and add server saved one
        dispatch(actions.remove(tmpResource.id));
        dispatch(actions.add(json));
        console.log('JSONN');
        console.log(json);
        browserHistory.push('/resources/' + json.url + '/edit');
      
        
      })
      .catch((err) => {
        actions.remove(tmpResource.id);
        alert(err.message); // eslint-disable-line no-alert
      });
    };
  },
  editAsync(resource) {
    //console.log(resource)
    console.log('edit async')
    return (dispatch) => {
  

      window.fetch('/api/resources/edit', {
        ...CONFIG_CLIENT.edit,
        method: 'put',
        body: JSON.stringify({ resource }),
      })
      .then((response) => response.json())
      .then((json) => {
        // remove optimistic resource and add server saved one
       
      })
      .catch((err) => {
        
        alert(err.message); // eslint-disable-line no-alert
      });
    };
  },
  publishToggle(resource) {
    //console.log(resource)

    return (dispatch) => {
      console.log('publish toggle')
      console.log(resource)
      // window.fetch('/api/resources/publish', {
      //   ...CONFIG_CLIENT.edit,
      //   method: 'put',
      //   body: JSON.stringify({ resource }),
      // })
      // // .then((response) => response.json())
      // .then((json) => {
      //   // remove optimistic resource and add server saved one
      //   dispatch(actions.remove(tmpResource.id));
      //   dispatch(actions.add(json));
      //   console.log(json)
      // })
      // .catch((err) => {
      //   actions.remove(tmpResource.id);
      //   alert(err.message); // eslint-disable-line no-alert
      // });
    };
  },
  filterResource(search) {
    let q = search.query ? search.query : 'all';
    return (dispatch) => {
       window.fetch(CONFIG_CLIENT.publicPath + 'api' +  '/resources/search/' + q, CONFIG_CLIENT.fetch)
      .then((response) => response.json())
      .then((json) => {
        // add resources fetched from the server
        dispatch(actions.load(json));
        //return json
      });
      // window.fetch(CONFIG_CLIENT.publicPath + 'api/resources/search', CONFIG_CLIENT.fetch)
      // .then((response) => response.json())
      // .then((json) => {
      //   // add resources fetched from the server
      //   dispatch(actions.load(json));
      // });
      console.log('SEARCH')
      console.log(q)


    };
    
  },

};

/**
 * Exports
 */

export { types, reducer, actions };
