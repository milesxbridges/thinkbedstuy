import { combineReducers } from 'redux';
import { sessionReducer } from 'redux-react-session';
import { modelReducer, formReducer, combineForms } from 'react-redux-form'


/**
 * Export all actions and the combined reducer
 */

import * as resource from 'providers/resource';
import * as authenticate from 'providers/authenticate';

export {
  resource,
  authenticate,
};

const initialResourceState = {
	businessName: '',
	url: '',
	address: '',
	interviewee: '',
	email: '',
	shortDescription: '',
	fullDescription: '',
	website: '',
	keywords: '',
	category: '',
	q1: '',
	q2: '',
	q3: '',
	q4: '',
	q5: '',
	q6: '',
	q7: '',
	q8: '',
	q9: '',
	q10: '',
	q11: '',
	q12: '',
	q13: '',
	q14: '',
	q15: '',
	q16: '',
	q17: '',
	q18: '',
	q19: '',
	q20: '',
	q21: '',
	published: false,
}
const editResourceState = {
	businessName: '',
	url: '',
	address: '',
	interviewee: '',
	email: '',
	shortDescription: '',
	fullDescription: '',
	website: '',
	keywords: '',
	category: '',
	q1: '',
	q2: '',
	q3: '',
	q4: '',
	q5: '',
	q6: '',
	q7: '',
	q8: '',
	q9: '',
	q10: '',
	q11: '',
	q12: '',
	q13: '',
	q14: '',
	q15: '',
	q16: '',
	q17: '',
	q18: '',
	q19: '',
	q20: '',
	q21: '',
	published: false,
}
const initialSearch = {
	
}
const initialLoginState = {
	username: '',
	password: '',
	admin: false
}

export default combineReducers({
	session: sessionReducer,
	resources: resource.reducer,
	deep: combineForms({
		resource: initialResourceState,
		resourceEdit: editResourceState,
		search: initialSearch,
		login: initialLoginState,
	}, 'deep')
});

