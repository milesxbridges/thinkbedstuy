/* eslint-disable complexity */
import _ from 'lodash';
import { createAction, handleActions } from 'redux-actions';
import { track, actions as act } from 'react-redux-form';
import { browserHistory } from 'react-router'
import cookie from 'react-cookie';  

/**
 * Action types
 */

const types = {
	AUTH_USER: 'AUTH_USER',
	UNAUTH_USER: 'UNAUTH_USER',
	FORGOT_PASSWORD_REQUEST: 'FORGOT_PASSWORD_REQUEST',
	RESET_PASSWORD_REQUEST: 'RESET_PASSWORD_REQUEST',
	PROTECTED_TEST: 'PROTECTED_TEST'
};


const defaultState = {};

const reducer = handleActions({

  [types.AUTH_USER](state, { payload }) {
    return payload;
  },

  [types.UNAUTH_USER](state, { payload }) {
    return payload;
  },

  [types.FORGOT_PASSWORD_REQUEST](state, { payload }) {
    return payload;
  },
  [types.RESET_PASSWORD_REQUEST](state, {payload}) {
    return payload;
  },
  [types.PROTECTED_TEST](state, { payload }) {
    return payload;
  }
}, defaultState);

const actions = {
	login(val) {
		console.log('login')
		console.log(val)
	},
	check() {
    // redux-thunk magic: it allow actions to return functions
    // w/ dispatch as argument, so we can call it as many times as needed
    return (dispatch) => {
      window.fetch(CONFIG_CLIENT.publicPath + 'api/login', CONFIG_CLIENT.fetch)
      // .then((response) => response.json())
      // .then((json) => {
      //   // add resources fetched from the server
      //   dispatch(actions.load(json));
      // });
    };
  }
}

export { types, actions, reducer };